var settings = require("./settings.json");
    settings.vignette_img = settings.vignette_img === "" ? "none" : "url(./../../img/" + settings.vignette_img + ")";
    settings.buttons_color = settings.buttons_color === "" ? "transparent" : settings.buttons_color;
    settings.buttons_color_selected = settings.buttons_color_selected === "" ? "transparent" : settings.buttons_color_selected;
    settings.border_color = settings.border_color === "" ? "transparent" : settings.border_color;
    settings.bg_color = settings.bg_color === "" ? "transparent" : settings.bg_color;
    settings.btf_color = settings.btf_color === "" ? "transparent" : settings.btf_color;
    settings.buttons_txt_color = settings.buttons_txt_color === "" ? "transparent" : settings.buttons_txt_color;
    settings.buttons_txt_color_selected = settings.buttons_txt_color_selected === "" ? "transparent" : settings.buttons_txt_color_selected;
    settings.buttons_police_size = settings.buttons_police_size === "" ? "12" : settings.buttons_police_size;
    settings.cta_normal_img = settings.cta_normal_img === "" ? "none" : "url(./../../img/" + settings.cta_normal_img + ")";
    settings.cta_hover_img = settings.cta_hover_img === "" ? "none" : "url(./../../img/" + settings.cta_hover_img + ")";
    settings.cta_color = settings.cta_color === "" ? "transparent" : settings.cta_color;
    settings.cta_bg_color = settings.cta_bg_color === "" ? "transparent" : settings.cta_bg_color;
    settings.cta2_normal_img = settings.cta2_normal_img === "" ? "none" : "url(./../../img/" + settings.cta2_normal_img + ")";
    settings.cta2_hover_img = settings.cta2_hover_img === "" ? "none" : "url(./../../img/" + settings.cta2_hover_img + ")";
    settings.cta2_color = settings.cta2_color === "" ? "transparent" : settings.cta2_color;
    settings.cta2_bg_color = settings.cta2_bg_color === "" ? "transparent" : settings.cta2_bg_color;

module.exports = function (source) {
    this.cacheable();
    var test = source
        .replace(/__PLACEHOLDER__/g, settings.format + settings.creaid)
        .replace(/__FORMAT__/g, settings.format)
        .replace(/__VIGNETTE_IMG__/g, settings.vignette_img)
        .replace(/__BORDER_COLOR__/g, settings.border_color)
        .replace(/__BG_COLOR__/g, settings.bg_color)
        .replace(/__BTFCOLOR__/g, settings.btf_color)
        .replace(/__BUTTONTXT__/g, settings.buttons_txt_color)
        .replace(/__BUTTONTXT_SEL__/g, settings.buttons_txt_color_selected)
        .replace(/__BUTTONCOLOR__/g, settings.buttons_color)
        .replace(/__BUTTONCOLOR_SEL__/g, settings.buttons_color_selected)
        .replace(/__BTF_CONTAINE_IMG__/g, settings.banner_container_img)
        .replace(/__BTF_CONTAINE_IMG_MOB__/g, settings.banner_container_img_mob)
        .replace(/__CTA_IMG__/g, settings.cta_normal_img)
        .replace(/__CTA_IMG_HOVER__/g, settings.cta_hover_img)
        .replace(/__CTA_COLOR__/g, settings.cta_color)
        .replace(/__CTA_BG_COLOR__/g, settings.cta_bg_color)
        .replace(/__CTA2_IMG__/g, settings.cta2_normal_img)
        .replace(/__CTA2_IMG_HOVER__/g, settings.cta2_hover_img)
        .replace(/__CTA2_COLOR__/g, settings.cta2_color)
        .replace(/__CTA2_BG_COLOR__/g, settings.cta2_bg_color);
    return test;
};
