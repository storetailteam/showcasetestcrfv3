"use strict";

const ctxt_pos = 0,
  ctxt_title = 1,
  ctxt_modal = 2,
  ctxt_prodlist = 3;

var sto = window.__sto,
  settings = require("./../../settings"),
  style = require("./main.css"),
  custom = settings.custom,
  productss = [],
  cta_position = settings.cta_position == '' ? 'bottom' : settings.cta_position,
  cta_target = settings.cta_target == '' ? '_blank' : settings.cta_target,
  classParent = "storetail-id-" + settings.format + settings.creaid,
  cta_size = settings.cta_size != '' ? settings.cta_size : 'small',
  option = settings.option,
  isViewFired = false,
  isIMPfired = false,
  format = settings.format;

var fontRoboto = document.createElement('link');
fontRoboto.setAttribute('rel', 'stylesheet');
fontRoboto.setAttribute('href', 'https://fonts.googleapis.com/css?family=Roboto');
var head = document.querySelector('head');
head.appendChild(fontRoboto);

custom.sort(function(a, b) {
  return a[ctxt_pos] > b[ctxt_pos]
});

// Get all product ids from custom product organization
custom.forEach(function(e) {
  e[ctxt_prodlist].forEach(function(p) {
    productss.push(p);
  })
});

module.exports = {
  init: _init_(),
}

var settings_Format = {
    "ids": productss,
    "type": settings.format,
    "creaId": settings.creaid
  },
  format_setting = [{
    "id": "0000000000",
    "type": "partner-multi-products",
    "partner": {
      "name": "storetail",
      "crea_id": settings.creaid,
      "crea_type": settings.format,
      "need_products": true,
      "class": "storetail sto-banner storetail-sr-vignette storetail-id-" + settings.format + settings.creaid,
      "position_reference": null,
    },
    "products": settings.custom.map(function(pdt, i) {
      return {
        "id": pdt[3][0],
        "type": "partner-product",
        "partner": {
          "name": "storetail",
          "crea_id": settings.creaid,
          "crea_type": settings.format,
          "class": "storetail storetail-sr-vignette storetail-id-" + settings.format + settings.creaid,
          "position_reference": null,
        }
      }
    })
  }];


function _init_() {

  try {

    var loadData = __sto.getTrackers("SC").length === undefined ? "SC" : ["SC", settings.creaid];

    sto.load(loadData, function(tracker) {

        var removers = {};
        removers[format] = function () {
          style.unuse();
          document.querySelector("." + classParent).remove();
        };


        if (window.matchMedia("(max-width: 1023.5px)").matches) {
          tracker.error({
            "tl": "mobileViewport"
          });
          return removers;
        }

        sto.utils.retailerMethod.crawlAPI(settings_Format).promise.then(function(result) {

          var avail = checkAvailability(result, settings.custom);
          if(avail[0][0].length < 4){
            tracker.availability({
              "tn":avail[0][0].length
            });
            return false;
          }

          sto.utils.retailerMethod.createFormat(tracker, format_setting, result);

          // Outside of onFormatAddedInDOM to prevent multiple onResize event after new format append into DOM
          window.addEventListener("resize", function(){
              var scContainer = document.querySelector("." + classParent);
              if(scContainer){
                widthProductBox(scContainer);
                dataCount(scContainer);
              }
          });

          document.addEventListener("onFormatAddedInDOM", function(e) {
              try {
                  var scNodeContainer = e && e["detail"] && e["detail"]["formatNode"] ? e["detail"]["formatNode"] : null;

                  if(scNodeContainer && scNodeContainer.className.indexOf(classParent) > -1){
                      style.use();
                      generateVignette(scNodeContainer);
                      widthProductBox(scNodeContainer);
                      optionsLink(scNodeContainer, tracker);
                      dataCount(scNodeContainer);

                      if (scNodeContainer.style.display != "none" && isIMPfired === false) {
                          tracker.display();
                          isIMPfired = true;
                      }

                      if (isViewFired === false) {
                          __sto.utils.addViewTracking("." + classParent, tracker);
                          isViewFired = true;
                      }
                  }
            } catch(e){
              console.log(e);
            }

          });
        });
    });
  } catch (e) {
    console.log(e);
  }
}


function optionsLink(scNodeContainer, tracker) {
  if (option != "none" && option != "") {
    var target = scNodeContainer.querySelector('.sto-vignette');
    scNodeContainer.setAttribute("cta-type", option);

    var cta = document.createElement('span');
    cta.className = 'sto-cta';
    cta.setAttribute('cta-pos', cta_position);
    cta.innerHTML = '<div class="sto-options-cta" cta-size=' + cta_size + '><span class="sto-cta-text">' + settings.cta_text + '</span></div>';
    target.appendChild(cta);


    switch (option) {
      case 'redirection':
        scNodeContainer.querySelector('.sto-cta').addEventListener('click', function() {
          tracker.click();
          window.open(settings.cta_redirect, cta_target);
        });

        break;
      case 'file':
        scNodeContainer.querySelector('.sto-cta').addEventListener('click', function() {
          tracker.openPDF();
          var pdf = require("../../img/" + settings.cta_file);
          window.open(pdf, "_blank");
        });
        scNodeContainer.querySelector('.sto-logo .sto-cta-img').addEventListener('click', function() {
          tracker.openPDF();
          var pdf = require("../../img/" + settings.cta_file);
          window.open(pdf, "_blank");
        });
        break;
      case 'video':
        scNodeContainer.querySelector('.sto-cta').addEventListener('click', function() {
          tracker.playVideo({
            "tl": "open"
          })
          var videoBackground = document.createElement('div');
          videoBackground.className = 'sto-ecran';
          var videoBox = document.createElement('div');
          videoBox.className = 'sto-video';
          videoBox.innerHTML = '<div class="sto-close"></div>' + settings.cta_embed_video + '</div>';
          document.querySelector('body').appendChild(videoBackground);
          document.querySelector('body').appendChild(videoBox);
          document.querySelector('.sto-close').addEventListener('click', function() {
            tracker.close({
              "tl": "closeBtn"
            });
            var elemVideo1 = document.querySelector(".sto-ecran");
            elemVideo1.parentNode.removeChild(elemVideo1);
            var elemVideo2 = document.querySelector(".sto-video");
            elemVideo2.parentNode.removeChild(elemVideo2);
          });
          document.querySelector('.sto-ecran').addEventListener('click', function() {
            tracker.close({
              "tl": "blackScreen"
            });
            var elemVideo1 = document.querySelector(".sto-ecran");
            elemVideo1.parentNode.removeChild(elemVideo1);
            var elemVideo2 = document.querySelector(".sto-video");
            elemVideo2.parentNode.removeChild(elemVideo2);
          });
        });
        break;
      default:
    }
  }

}

function generateVignette(scNodeContainer){
    var productsContainer = scNodeContainer.querySelector(".product-card").parentElement;
    productsContainer.className = "sto-products-container";

    if(!scNodeContainer.querySelector(".sto-vignette")){
        var vignette = document.createElement('div');
        vignette.className = "sto-vignette";
        scNodeContainer.prepend(vignette);
    }
}

function widthProductBox(scNodeContainer) {
    var getWidthProduct = document.querySelectorAll('.grid .grid-item')[0].getBoundingClientRect().width,
    formatProductCard = scNodeContainer.querySelectorAll('.sto-products-container .product-card'),
    formatVignette = scNodeContainer.querySelector('.sto-vignette');

    formatVignette.style.width = getWidthProduct + "px";
    for (var i = 0; i < formatProductCard.length; i++) {
      formatProductCard[i].style.width = getWidthProduct + "px";
    }
}

function dataCount(scNodeContainer) {
  if (window.matchMedia("(max-width: 598px)").matches) {
    scNodeContainer.setAttribute('data-count', 0);
    scNodeContainer.setAttribute('data-viewport', 'mobile');
  } else if (window.matchMedia("(max-width: 1023.5px)").matches) {
    scNodeContainer.setAttribute('data-count', 1);
    scNodeContainer.setAttribute('data-viewport', 'desktop');
  } else if (window.matchMedia("(max-width: 1279.5px)").matches) {
    scNodeContainer.setAttribute('data-count', 2);
  } else if (window.matchMedia("(max-width: 1439.5px)").matches) {
    scNodeContainer.setAttribute('data-count', 3);
  } else {
    scNodeContainer.setAttribute('data-count', 4);
  }
}

function checkAvailability(crawledProds, products) {
  var realProducts = [],
    temporary = [
      [],
      [],
      []
    ],
    rowModal = 0,
    toggleModal = true,
    modalMode = false;

  products.forEach(function (thisProductLine, i) {
    var toggleRowProduct = true;
    thisProductLine[3].forEach(function (thisProduct, j) {
      if (toggleRowProduct === true && Object.keys(crawledProds).includes(thisProduct)) {
        rowModal++;
        toggleRowProduct = false;
        temporary[0].push(thisProduct);
        temporary[1].push(thisProductLine[1]);
        temporary[2].push(thisProductLine[0]);
      }
    });
    if (thisProductLine[2] == true) {
      modalMode = true;
      if (rowModal <= 0) {
        toggleModal = false;
      }
    }
    rowModal = 0;
  });
  realProducts[0] = temporary[0];
  realProducts[1] = temporary[1];
  realProducts[2] = temporary[2];
  return (modalMode == true ? toggleModal == false ? [realProducts, false] : [realProducts, true] : [realProducts])
}